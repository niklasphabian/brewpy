import requests
from subprocess import call
import datetime
import time
import os
import csv
import sys
import RPi.GPIO as GPIO
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

os.system('lighttpd -f lighttpd.conf')

class Stepper():
    def __init__(self):
        GPIO.setmode(GPIO.BCM)
        self.pins = [17,27,22,18]
        self.setupPins()
        self.seq = [[1,0,0,1],
	       [1,0,0,0],
	       [1,1,0,0],
	       [0,1,0,0],
	       [0,1,1,0],
	       [0,0,1,0],
	       [0,0,1,1],
	       [0,0,0,1]]
        self.read_wait_time()

    def read_wait_time(self):
        if len(sys.argv)>1:
            self.wait_time = int(sys.argv[1])/float(2000)
        else:
            self.wait_time = 0.002

    def setupPins(self):
        for pin in self.pins:	    	    
            GPIO.setup(pin,GPIO.OUT)
            GPIO.output(pin, False)

    def move_right(self, steps):
        for step in range(steps):
            for row in self.seq:
                GPIO.output(self.pins[0], row[0])
                GPIO.output(self.pins[1], row[1])
                GPIO.output(self.pins[2], row[2])
                GPIO.output(self.pins[3], row[3])
                time.sleep(self.wait_time)

    def move_left(self, steps):
        for step in range(steps):
            for row in reversed(self.seq):
                GPIO.output(self.pins[0], row[0])
                GPIO.output(self.pins[1], row[1])
                GPIO.output(self.pins[2], row[2])
                GPIO.output(self.pins[3], row[3])
                time.sleep(self.wait_time)


class TempSensor():
    def __init__(self):
        os.system('modprobe w1-gpio')
        os.system('modprobe w1-therm')
        self.temp_sensor = '/sys/bus/w1/devices/28-0000075d1842/w1_slave'

    def temp(self):
        with open(self.temp_sensor, 'r') as tmpfile:
            temp = float(tmpfile.readlines()[1][-6:-1])/1000
        return temp


class Controller:
    def __init__(self):
        self.state = 'low'
        self.stepper = Stepper()
   
    def switch(self, temp, setTemp):
        if temp > setTemp:
            self.set_state_low()
        elif temp < setTemp:
            self.set_state_high()
        else:
            pass  

    def set_state_low(self):
        if self.state == 'low':
            pass
        else :
            print('Temperature high; reducing power')
            self.turn_left()
            self.state = 'low'

    def set_state_high(self):
        if self.state == 'high': 
            pass
        else :
            print('Temperature low; increasing power')
            self.turn_right()
            self.state = 'high'

    def turn_left(self):
        self.stepper.move_left(500)

    def turn_right(self):
        self.stepper.move_right(500)
         
    def setTemp(self, targetTemp) :
        self.targetTemp = targetTemp

    def setDuration(self, restTime):
        self.endTime = datetime.datetime.now() + datetime.timedelta(seconds=restTime)

    def doRest(self):
        while datetime.datetime.now() < self.endTime:
            temp = round(sensor.temp(),1)
            print('{time}: Temp is {temp} degC'.format(temp=temp, time=datetime.datetime.now()))
            graph.drawActual(temp)
            self.switch(temp, self.targetTemp)
            time.sleep(1)


class Graph:
    def __init__(self):        
        self.fig = plt.figure()        
        self.ax = self.fig.add_subplot(111)
        self.ax.set_ylabel('temperature in degC')
        self.ax.set_xlabel('time in second')
        self.ax.grid()
        plt.ion()
        plt.ylim([10,100])
        plt.savefig('www/brewpy.png')      
        self.startTime = datetime.datetime.now()

    def drawActual(self, temp):
        now = datetime.datetime.now()
        self.absSecs = (now - self.startTime).total_seconds()
        self.ax.scatter(self.absSecs, temp, color='r')
        plt.draw()
        plt.savefig('www/brewpy.png')

    def drawTarget(self, temp, duration):
        now = datetime.datetime.now()
        self.absSecs = (now - self.startTime).total_seconds()
        end = self.absSecs + duration
        self.ax.plot([self.absSecs, end], [temp, temp], 'b')
        plt.draw()
        plt.savefig('www/brewpy.png')

    def sustain(self):
        plt.ioff()
        plt.savefig('www/brewpy.png')


def readRestList(fname):
    with open(fname, 'r') as inFile:
        csvReader = csv.reader(inFile, delimiter=',')
        next(csvReader)
        next(csvReader)
        restList = []
        for row in csvReader:
            restList.append([row[0], int(row[1]), int(row[2])])
        return restList


def main():
    print('beginning')
    restList = readRestList('program.csv')    
    global graph 
    global sensor
    global controller
    graph = Graph() 
    sensor = TempSensor()
    controller = Controller()

    for rest in restList:
        rest_name = rest[0]
        targetTemp = rest[1]
        duration = rest[2]  
        print('Doing rest {}: T={} degC, t={} sec'.format(rest_name, targetTemp, duration))        
        controller.setTemp(targetTemp)
        controller.setDuration(duration)
        graph.drawTarget(targetTemp, duration)
        controller.doRest()
    graph.sustain()




if __name__ == '__main__':    
    main()


